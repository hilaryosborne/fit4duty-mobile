import React from 'react';

import { Route, Link } from 'react-router-dom';

import { connect } from 'react-redux';

import { Page as DashboardPage } from './dashboard/page';
import { Page as PreparePage } from './prepare/page';
import { Page as DepartPage } from './depart/page';
import { Page as SignoffPage } from './signoff/page';
import { Page as IncidentPage } from './incident/page';
import { Page as TravelPage } from './travel/page';

import { Router as TestingRouter } from './testing/router';

class ContainerComponent extends React.Component {
	render() {
		return(<div>
			{this.props.children}
			<hr />
			<div className="card">
				<div className="card-block p-2">
					<ul className="list-group">
						<li className="d-flex py-1 px-2 list-group-item active">Information Shortcuts</li>
						<li className="d-flex py-1 px-2 list-group-item">Client Organisation Details<span className="ml-auto">x</span></li>
						<li className="d-flex py-1 px-2 list-group-item">Client Representative Details<span className="ml-auto">x</span></li>
					</ul>
				</div>
			</div>
		</div>);
	}
}

const Container = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
	};
})(ContainerComponent);

export const Router = ({ match }) => (
	<Container>
		<div>
			<Route exact path={match.url} component={DashboardPage} />
			<Route exact path={`${match.url}/prepare`} component={PreparePage} />
			<Route exact path={`${match.url}/depart`} component={DepartPage} />
			<Route exact path={`${match.url}/signoff`} component={SignoffPage} />
			<Route exact path={`${match.url}/incident`} component={IncidentPage} />
			<Route exact path={`${match.url}/travel`} component={TravelPage} />
			<Route path={`${match.url}/testing/:uuid`} component={TestingRouter} />
		</div>
	</Container>
);