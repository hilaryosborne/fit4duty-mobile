import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';

import { connect } from 'react-redux';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import {getSessionCollector} from "../functions";

export const Dashboard = connect((state)=>{
	return {
		profile:_.get(state,'profile',{}),
		session:_.get(state,'session',{}),
		collector:getSessionCollector(_.get(state,'session.stream'),_.get(state,'profile.stream'))
	};
})(class Dashboard extends React.Component {

	constructor(props){
		super(props);
		this.state = {};
	}

	toggleTaskGroup(group) {
		const session = {...this.props.session};
		const collector = this.props.collector;
		_.set(collector,'progress.'+group+'.active',!_.get(collector,'progress.'+group+'.active',false));
		this.props.dispatch({type:'UPDATE_SESSION',payload:session,transmit:[]});
	}

	isGroupComplete(group) {
		const collector = this.props.collector;
		return !_.find(_.get(collector,'progress.'+group+'.steps',[]),(steps)=>{
			return !steps.complete;
		});
	}

	countGroupSteps(group) {
		const collector = this.props.collector;
		return _.size(_.get(collector,'progress.prepare.steps',[]));
	}

	countCompletedGroupSteps(group) {
		const collector = this.props.collector;
		return _.countBy(_.get(collector,'progress.prepare.steps',[]));
	}

	render() {
		return (<div className="py-2">
			<div className="card my-3">
				<div className="card-header p-2">
					<h4 className="mb-0">Session Task Checklist</h4>
				</div>
				<div className="card-block p-2">
					<ul className="list-group mb-2">
						<li className={classNames("d-flex py-1 px-2 list-group-item",{"active":!this.isGroupComplete('prepare'), "list-group-item-success":this.isGroupComplete('prepare')})}  onClick={()=>{
							this.toggleTaskGroup('prepare');
						}}>Prepare for the job<span className="ml-auto">[0/{this.countGroupSteps('prepare')}]</span></li>
						{_.get(this.props,'collector.progress.prepare.active') ? [
							<li key={0} className="d-flex py-1 px-2 list-group-item">Equipment Check<span className="ml-auto">x</span></li>,
							<li key={1} className="d-flex py-1 px-2 list-group-item">Courtesy Call<span className="ml-auto">x</span></li>
						] : false}
					</ul>
					<ul className="list-group mb-2">
						<li className="d-flex py-1 px-2 list-group-item active" onClick={()=>{
							this.toggleTaskGroup('prepare');
						}}>Depart for the job<span className="ml-auto">[0/3]</span></li>
					</ul>
					<ul className="list-group mb-2">
						<li className="d-flex py-1 px-2 list-group-item active" onClick={()=>{
							this.toggleTaskGroup('prepare');
						}}>Arrive at the job<span className="ml-auto">[0/3]</span></li>
					</ul>
					<ul className="list-group mb-2">
						<li className="d-flex py-1 px-2 list-group-item active" onClick={()=>{
							this.toggleTaskGroup('prepare');
						}}>Conduct Testing<span className="ml-auto">[0/3]</span></li>
					</ul>
					<ul className="list-group">
						<li className="d-flex py-1 px-2 list-group-item active" onClick={()=>{
							this.toggleTaskGroup('prepare');
						}}>Complete the job<span className="ml-auto">[0/3]</span></li>
					</ul>
				</div>
			</div>
			<div className="card">
				<div className="card-block p-2">
					<button className="btn btn-block btn-danger" onClick={()=>{ this.props.onSelect('onincident'); }}>Report An Incident</button>
					<button className="btn btn-block btn-danger" onClick={()=>{ this.props.onSelect('onincident'); }}>Report Broken Equipment</button>
				</div>
			</div>
			<Modal size="md" isOpen={_.get(this.state,'modals.prepare.show',false)} toggle={()=>{ this.setState(_.set({...this.state},'modals.prepare.show', false)); }}>
				<ModalBody>
					<div>Yey</div>
				</ModalBody>
			</Modal>
		</div>);
	}

});

