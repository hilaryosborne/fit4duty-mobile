import React from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';

export const Communication = connect((state)=>{
	return {
		profile:_.get(state,'profile',{}),
		session:_.get(state,'session',{})
	};
})(class Communication extends React.Component {

	constructor(props){
		super(props);
		this.state = {};
	}

	getCommunications() {
		return _.get(this.props,'session.stream.data.communications',[]);
	}

	render() {
		return (<div>
			<List onSelect={(selected)=>{ console.log(selected); }} />
		</div>);
	}

});

class List extends React.Component {

	defaultProps = {
		onSelect:_.noop
	};

	constructor(props){
		super(props);
		this.state = {};
	}

	render() {
		return (<div className="py-2">
			<div className="card my-3">
				<div className="card-header p-2">
					<h4 className="mb-0">What are you about to do?</h4>
				</div>
				<div className="card-block p-2">
					<button className="btn btn-block btn-primary" onClick={()=>{ this.props.onSelect('onprepare'); }}>Preparing for the job</button>
					<button className="btn btn-block btn-primary" onClick={()=>{ this.props.onSelect('ondeparture'); }}>Departing for the job</button>
					<button className="btn btn-block btn-primary" onClick={()=>{ this.props.onSelect('onarrival'); }}>Arriving at the job</button>
					<button className="btn btn-block btn-primary" onClick={()=>{ this.props.onSelect('onarrival'); }}>Conduct Testing</button>
					<button className="btn btn-block btn-primary" onClick={()=>{ this.props.onSelect('oncompletion'); }}>Completing the job</button>
				</div>
			</div>
			<div className="card">
				<div className="card-header p-2">
					<h4 className="mb-0">Non-Scheduled Communication</h4>
				</div>
				<div className="card-block p-2">
					<button className="btn btn-block btn-danger" onClick={()=>{ this.props.onSelect('onincident'); }}>Report Incident</button>
					<button className="btn btn-block btn-danger" onClick={()=>{ this.props.onSelect('onincident'); }}>Report Broken Equipment</button>
				</div>
			</div>
		</div>);
	}

}