import React from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';

import { Overview as SessionOverview } from './overview';
import { Communication as SessionCommunication } from './communication';
import { Dashboard as SessionDashboard } from './dashboard';
import { Arrival as SessionArrival } from './arrival';
import { Checks as SessionChecks } from './checks';
import { Testing as SessionTesting } from './testing';
import { Signoff as SessionSignoff } from './signoff';

import { getSessionCollector } from "../functions";

export const Form = connect((state)=>{
	return {
		profile:_.get(state,'profile',{}),
		session:_.get(state,'session',{})
	};
})(class Form extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			current:'overview'
		};
	}

	render() {
		return (<div>
			{_.get(getSessionCollector(_.get(this.props,'session.stream'),_.get(this.props,'profile.stream')),'status','pending') === 'pending' ?
				<SessionOverview /> : false }
			{_.get(getSessionCollector(_.get(this.props,'session.stream'),_.get(this.props,'profile.stream')),'status','pending') === 'accepted' ?
				<div>
					<SessionDashboard />
				</div> : false }
			{_.get(getSessionCollector(_.get(this.props,'session.stream'),_.get(this.props,'profile.stream')),'status','pending') === 'rejected' ?
				<div>You have rejected this job!</div> : false }
		</div>);
	}

});
