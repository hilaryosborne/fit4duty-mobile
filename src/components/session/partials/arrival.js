import React from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';

export const Arrival = connect((state)=>{
	return {
		profile:_.get(state,'profile',{}),
		session:_.get(state,'session',{})
	};
})(class Arrival extends React.Component {

	constructor(props){
		super(props);
		this.state = {};
	}

	render() {
		return (<div>On Arrival</div>);
	}

});

