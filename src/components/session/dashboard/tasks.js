import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';

import { connect } from 'react-redux';

class TasksComponent extends React.Component {

	state = {};

	render() { console.log(this.props);
		return (<div>
			<div className="mb-3">
				<h3 className="d-flex">Testing Session</h3>
				<div>#{_.get(this.props,'session.loaded._uuid')}</div>
				<hr className="my-3" />
			</div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Session Schedule</h4>
					<hr />
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt leo at euismod dictum. Nulla ultrices tortor quam, ac molestie lectus placerat sed.</p>
					<ul className="list-group">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('prepare')); }}>Prepare for the job<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('depart')); }}>Conduct Hazard Assessment<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('arrive')); }}>Conduct Donor Selection<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('conduct')); }}>Conduct Donor Testing<span className="ml-auto">!</span></li>
					</ul>
				</div>
			</div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Logs And Reporting</h4>
					<hr />
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt leo at euismod dictum. Nulla ultrices tortor quam, ac molestie lectus placerat sed.</p>
					<ul className="list-group mb-2">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('prepare')); }}>Log Travel To Site<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('prepare')); }}>Log Travel From Site<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('prepare')); }}>Log Break Periods<span className="ml-auto">!</span></li>
					</ul>
					<ul className="list-group">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('prepare')); }}>Report An Incident<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('depart')); }}>Report Broken Equipment<span className="ml-auto">!</span></li>
					</ul>
				</div>
			</div>
			<div className="card">
				<div className="card-block p-2">
					<h4>Session Signoff</h4>
					<hr />
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt leo at euismod dictum. Nulla ultrices tortor quam, ac molestie lectus placerat sed.</p>
					<ul className="list-group">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('prepare')); }}>Completed all required tests<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('depart')); }}>Collected all waste matter<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('depart')); }}>Packed and correctly stored equipment<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('depart')); }}>Liaised with appropriate site staff<span className="ml-auto">!</span></li>
					</ul>
					<hr />
					<ul className="list-group mb-2">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('prepare')); }}>Notify Client Representative<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('depart')); }}>Notify Home Office<span className="ml-auto">!</span></li>
					</ul>
					<button className="btn btn-block btn-lg btn-success">Conclude Session</button>
				</div>
			</div>
		</div>);
	}

}

export const Tasks = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
		event:_.get(state,'session.event',false)
	};
})(TasksComponent);

