import React from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class SummaryComponent extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			modals:{
				reject:{show:false}
			}
		};
	}

	doAcceptAction() {
		this.setState(_.set({...this.state},'modals.accept.show', false),()=>{
			const collector = {...this.props.collector};
			_.set(collector,'status','accepted');
			this.props.dispatch({type:'UPDATE_COLLECTOR',payload:collector,transmit:["COLLECTOR:ACCEPTED"]});
		});
	}

	doRejectAction() {
		this.setState(_.set({...this.state},'modals.reject.show', false),()=>{
			const collector = {...this.props.collector};
			_.set(collector,'status','rejected');
			this.props.dispatch({type:'UPDATE_COLLECTOR',payload:collector,transmit:["COLLECTOR:REJECTED"]});
		});
	}

	render() {
		return (<div>
			<div className="card">
				<div className="card-header p-2">
					<h4 className="mb-0">Schedule Details</h4>
				</div>
				<div className="card-block p-2">
					<div className="d-flex">
						<strong>Proposed Date:</strong><span className="ml-auto">12/05/2017</span>
					</div>
					<div className="d-flex">
						<strong>Proposed Start Time:</strong><span className="ml-auto">12:30pm</span>
					</div>
					<div className="d-flex">
						<strong>Duration:</strong><span className="ml-auto">4 hours</span>
					</div>
					<div className="d-flex">
						<strong>Collectors Allocated:</strong><span className="ml-auto">2</span>
					</div>
				</div>
			</div>
			<div className="card my-3">
				<div className="card-header p-2">
					<h4 className="mb-0">Client Details</h4>
				</div>
				<div className="card-block p-2">
					<strong>BlockQuote Pty Ltd</strong>
					<p>47 Dan Street, Graceville, Brisbane, QLD</p>
				</div>
			</div>
			<div className="card">
				<div className="card-header p-2">
					<h4 className="mb-0">Testing Details</h4>
				</div>
				<div className="card-block p-2">
					<div className="d-flex">
						<strong>Expected Staff:</strong><span className="ml-auto">45</span>
					</div>
					<div className="d-flex">
						<strong>Drug Matrix:</strong><span className="ml-auto">Urine, Saliva</span>
					</div>
				</div>
			</div>
			<div className="d-flex mt-3">
				<button className="btn btn-success" onClick={()=>{ this.setState(_.set({...this.state},'modals.accept.show', true)); }}>Accept Session</button>
				<button className="btn btn-danger ml-auto" onClick={()=>{ this.setState(_.set({...this.state},'modals.reject.show', true)); }}>Reject Session</button>
			</div>
			<Modal size="sm" isOpen={_.get(this.state,'modals.reject.show',false)} toggle={()=>{ this.setState(_.set({...this.state},'modals.reject.show', false)); }}>
				<ModalBody>
					<button onClick={()=>{ this.doRejectAction(); }} className="btn btn-block btn-lg btn-danger">Confirm</button>
					<button onClick={()=>{ this.setState(_.set({...this.state},'modals.reject.show', false)); }} className="btn btn-block btn-secondary">Cancel</button>
				</ModalBody>
			</Modal>
			<Modal size="sm" isOpen={_.get(this.state,'modals.accept.show',false)} toggle={()=>{ this.setState(_.set({...this.state},'modals.accept.show', false)); }}>
				<ModalBody>
					<button onClick={()=>{ this.doAcceptAction(); }} className="btn btn-block btn-lg btn-success">Confirm</button>
					<button onClick={()=>{ this.setState(_.set({...this.state},'modals.accept.show', false)); }} className="btn btn-block btn-secondary">Cancel</button>
				</ModalBody>
			</Modal>
		</div>);
	}

}

export const Summary = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
	};
})(SummaryComponent);

