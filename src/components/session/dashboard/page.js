import React from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';
import { Summary as DashboardSummary } from './summary';
import { Tasks as DashboardTasks } from './tasks';

import SESSION_STREAM from '../../../data/job.json';

const EMPTY_SESSION = ()=>{
	return {
		stream:false,
		collector:false,
		event:false
	};
};

class PageComponent extends React.Component {

	state = {};

	componentDidMount() {
		const session = EMPTY_SESSION();
		_.set(session,'stream',SESSION_STREAM);
		_.set(session,'collector',this.getSessionCollector(SESSION_STREAM));
		_.set(session,'event',false);
		this.props.dispatch({type:'LOAD_SESSION',payload:session});
	}

	getSessionCollector(session) {
		return _.find(_.get(session,'data.collectors',[]),(_collector)=>{
			return _.get(_collector,'staff_uuid',false) === _.get(this.props,'profile.stream.data._uuid');
		});
	};

	render() {
		return (<div>{_.get(this.props,'session.stream',false) ? <DashboardTasks /> :<div>Not Loaded</div> }</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false)
	};
})(PageComponent);

