import React from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';

class PageComponent extends React.Component {

	constructor(props){
		super(props);
		this.state = {};
	}

	render() {
		return (<div>I is page</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',{}),
		session:_.get(state,'session',{})
	};
})(PageComponent);

