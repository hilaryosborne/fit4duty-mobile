import React from 'react';
import _ from 'lodash';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class PageComponent extends React.Component {

	state = {};

	render() {
		return (<div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4 className="mb-2">Medical Declaration</h4>
					<div className="card mb-2">
						<div className="card-block p-2">
							<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et enim eros. Morbi euismod non quam ac facilisis. Nunc vel dolor sed nisi mollis aliquet. Proin varius dapibus ligula sit amet pulvinar. In dictum tellus interdum viverra pharetra. Ut sagittis semper tellus, eu pellentesque metus ornare ut. Vestibulum volutpat enim nec pulvinar hendrerit. Nulla convallis varius scelerisque. Sed eu dictum enim. Morbi congue vulputate odio, at ornare erat porttitor sed. Pellentesque tortor arcu, eleifend dictum iaculis eu, ultricies at est.</small>
							<ul className="list-group mt-2">
								<li className="d-block py-1 px-2 list-group-item">
									<div className="d-flex">ibprofen 50mg<span className="ml-auto">Remove</span></div>
									<small className="d-flex">Amount Taken: 150mg Last Taken: 12/07/2017</small>
								</li>
								<li className="d-block py-1 px-2 list-group-item">
									<div className="d-flex">panadol 50mg<span className="ml-auto">Remove</span></div>
									<small className="d-flex">Amount Taken: 150mg Last Taken: 12/07/2017</small>
								</li>
							</ul>
						</div>
					</div>
					<button className="btn btn-block btn-primary">Add Medication</button>
				</div>
			</div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4 className="mb-2">Medical Declaration Notes</h4>
					<div className="card mb-2">
						<div className="card-block px-2 py-1">
							<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies lectus sed leo rutrum congue. Duis sit amet viverra ante. Donec scelerisque nisl a semper pretium. </small>
						</div>
					</div>
					<button className="btn btn-block btn-primary">Add Note</button>
				</div>
			</div>
			<div className="card">
				<div className="card-block p-2">
					<div className="row">
						<div className="col col-12 col-sm-4">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.props.dispatch(push('personal'));
							}}>Back</button>
						</div>
						<div className="col col-12 col-sm-8">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.props.dispatch(push('.'));
							}}>Ok, Continue</button>
						</div>
					</div>
				</div>
			</div>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
		event:_.get(state,'session.event',false),
		test:_.get(state,'session.test',false),
	};
})(PageComponent);

