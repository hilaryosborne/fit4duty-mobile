import React from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';

class PassiveComponent extends React.Component {

	state = {};

	render() {
		return (<div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<div className="form-group row">
						<div className="col col-12 col-sm-6">
							<label>Active Test Result</label>
							<select className="form-control">
								<option>Pass</option>
								<option>Failed</option>
								<option>Inconclusive</option>
							</select>
						</div>
						<div className="col col-12 col-sm-6">
							<label>Test Time</label>
							<input type="text" className="form-control" />
						</div>
					</div>
					<div className="form-group mb-0">
						<label>Breathalyser Serial#:</label>
						<div className="d-flex mb-1">
							<input type="text" className="form-control" />
							<button className="btn btn-primary ml-2">Lookup</button>
						</div>
					</div>
					<div className="card mt-2">
						<div className="card-block p-2">
							<h4 className="mb-0">FC5 Hornet Alcohol Screener</h4>
							<small><strong>Serial Num:</strong> #MFR5-FDFDS <strong>Last Recal:</strong> 04/03/2017 <strong>Due:</strong> 08/08/2017</small>
						</div>
					</div>
				</div>
			</div>
		</div>);
	}

}

export const Passive = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
		event:_.get(state,'session.event',false),
		test:_.get(state,'session.test',false),
	};
})(PassiveComponent);

