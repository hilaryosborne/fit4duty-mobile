import React from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class ListComponent extends React.Component {

	state = {};

	render() {
		return (
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4 className="mb-2">Laboratory Tests</h4>
					<hr />
					<p><strong>REQUIREMENTS</strong> It is required that you take an active alcohol test. If the active test comes back positive you are to conduct a passive test.</p>
					<ul className="list-group mb-2">
						<li className="d-block py-1 px-2 list-group-item">
							<div className="d-flex">Packaging<span className="ml-auto">Security Seal: Y323231212</span></div>
						</li>
						<li className="d-block py-1 px-2 list-group-item">
							<div className="d-flex">Packaging<span className="ml-auto">Security Seal: Y323231212</span></div>
						</li>
					</ul>
					<button className="btn btn-block btn-primary" onClick={()=>{
						this.setState(_.set({...this.state},'modals.create.show',true));
					}}>Add Security Packaging</button>
					<hr />
					<button className="btn btn-block">Add Donor Signature</button>
					<button className="btn btn-block">Add Collector Signature</button>
				</div>
				<Modal size="md" isOpen={_.get(this.state,'modals.create.show',false)} toggle={()=>{ this.setState(_.set({...this.state},'modals.create.show', false)); }}>
					<ModalBody>
						<div>Yey</div>
					</ModalBody>
				</Modal>
			</div>
		);
	}

}

export const List = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
		event:_.get(state,'session.event',false),
		test:_.get(state,'session.test',false),
	};
})(ListComponent);

