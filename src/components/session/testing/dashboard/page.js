import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import { List as AlcoholList } from '../alcohol/list';
import { List as DrugList } from '../drug/list';
import { List as LaboratoryList } from '../laboratory/list';

class PageComponent extends React.Component {

	constructor(props){
		super(props);
		this.state = {};
	}

	render() {
		return (<div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<ul className="list-group">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('personal')); }}>Personal Information<span className="ml-auto">x</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('medication')); }}>Medication Declaration<span className="ml-auto">x</span></li>
					</ul>
				</div>
			</div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Donor Consent Statement</h4>
					<div className="card mb-2">
						<div className="card-block px-2 py-1">
							<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et enim eros. Morbi euismod non quam ac facilisis. Nunc vel dolor sed nisi mollis aliquet. Proin varius dapibus ligula sit amet pulvinar. In dictum tellus interdum viverra pharetra. Ut sagittis semper tellus, eu pellentesque metus ornare ut. Vestibulum volutpat enim nec pulvinar hendrerit. Nulla convallis varius scelerisque. Sed eu dictum enim. Morbi congue vulputate odio, at ornare erat porttitor sed. Pellentesque tortor arcu, eleifend dictum iaculis eu, ultricies at est.</small>
						</div>
					</div>
					<button className="btn btn-block">Add Donor Signature</button>
				</div>
			</div>
			<AlcoholList />
			<DrugList />
			<LaboratoryList />
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Collector Statement</h4>
					<div className="card mb-2">
						<div className="card-block px-2 py-1">
							<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et enim eros. Morbi euismod non quam ac facilisis. Nunc vel dolor sed nisi mollis aliquet. Proin varius dapibus ligula sit amet pulvinar. In dictum tellus interdum viverra pharetra. Ut sagittis semper tellus, eu pellentesque metus ornare ut. Vestibulum volutpat enim nec pulvinar hendrerit. Nulla convallis varius scelerisque. Sed eu dictum enim. Morbi congue vulputate odio, at ornare erat porttitor sed. Pellentesque tortor arcu, eleifend dictum iaculis eu, ultricies at est.</small>
						</div>
					</div>
					<button className="btn btn-block">Add Collector Signature</button>
				</div>
			</div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Test Event Notes</h4>
					<div className="card mb-2">
						<div className="card-block px-2 py-1">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies lectus sed leo rutrum congue. Duis sit amet viverra ante. Donec scelerisque nisl a semper pretium. </p>
						</div>
					</div>
					<button className="btn btn-block btn-primary">Add Note</button>
				</div>
			</div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<button className="btn btn-block d-flex">Donor Details Are Correct</button>
					<button className="btn btn-block d-flex">Donor Consent Is Complete</button>
					<button className="btn btn-block d-flex">All Tests Are Complete</button>
					<button className="btn btn-block d-flex">All Laboratory Samples Packaged</button>
					<button className="btn btn-block d-flex">All Signatures Collected</button>
					<hr />
					<button className="btn btn-block btn-lg btn-success">Conclude Event</button>
				</div>
			</div>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
	};
})(PageComponent);

