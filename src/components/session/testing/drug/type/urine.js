import React from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';

class UrineComponent extends React.Component {

	state = {};

	render() {
		return (<div>
			<div className="card mb-2">
				<div className="card-block">
					<h4 className="mb-2">Urine Sample Integrity</h4>
					<div className="row mb-2">
						<div className="col col-12 col-sm-2">
							<label>OXI:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
						<div className="col col-12 col-sm-2">
							<label>CRE:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
						<div className="col col-12 col-sm-2">
							<label>pH:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
						<div className="col col-12 col-sm-2">
							<label>XXX:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
						<div className="col col-12 col-sm-2">
							<label>XXY:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
						<div className="col col-12 col-sm-2">
							<label>XXZ:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
					</div>
					<div className="row mb-2">
						<div className="col col-12 col-sm-4">
							<label>Sample Temp:</label>
							<input type="text" className="form-control"/>
						</div>
						<div className="col col-12 col-sm-4">
							<label>Ambient Temp:</label>
							<input type="text" className="form-control"/>
						</div>
						<div className="col col-12 col-sm-4">
							<label>Visual Check ok?</label>
							<select className="form-control">
								<option>Yes</option>
								<option>No</option>
							</select>
						</div>
					</div>
					<div className="form-group mb-0">
						<label>Testing Kit Used:</label>
						<div className="d-flex mb-1">
							<input type="text" className="form-control" />
							<button className="btn btn-primary ml-2">Lookup</button>
							<button className="btn btn-primary ml-2">Create</button>
						</div>
					</div>
					<div className="card mt-2">
						<div className="card-block p-2">
							<h4 className="mb-0">Urine Drug Test Kit</h4>
							<small><strong>Serial Num:</strong> #MFR5-FDFDS <strong>Exp Date:</strong> 04/03/2017</small>
						</div>
					</div>
				</div>
			</div>
			<div className="card mb-2">
				<div className="card-block">
					<h4 className="mb-2">Urine Drug Screening</h4>
					<div className="row mb-2">
						<div className="col col-12 col-sm-2">
							<label>COC:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
						<div className="col col-12 col-sm-2">
							<label>AMP:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
						<div className="col col-12 col-sm-2">
							<label>MET:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
						<div className="col col-12 col-sm-2">
							<label>THC:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
						<div className="col col-12 col-sm-2">
							<label>MOR:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
						<div className="col col-12 col-sm-2">
							<label>BZO:</label>
							<select className="form-control">
								<option>N</option>
								<option>L</option>
							</select>
						</div>
					</div>
					<div className="form-group row mb-0">
						<div className="col col-12 col-sm-6">
							<label>Laboratory Testing</label>
							<button className="btn btn-block">Yes, Required</button>
						</div>
						<div className="col col-12 col-sm-6">
							<label>Test Time</label>
							<input type="text" className="form-control" />
						</div>
					</div>
				</div>
			</div>
		</div>);
	}

}

export const Urine = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
		event:_.get(state,'session.event',false),
		test:_.get(state,'session.test',false),
	};
})(UrineComponent);

