import React from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { Saliva as TypeSaliva } from './type/saliva';
import { Urine as TypeUrine } from './type/urine';

class PageComponent extends React.Component {

	state = {};

	render() {
		return (<div>
			<TypeSaliva />
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Testing Notes</h4>
					<div className="card mb-2">
						<div className="card-block p-2">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies lectus sed leo rutrum congue. Duis sit amet viverra ante. Donec scelerisque nisl a semper pretium. </p>
						</div>
					</div>
					<button className="btn btn-block btn-primary">Add Note</button>
				</div>
			</div>
			<div className="card">
				<div className="card-block p-2">
					<button className="btn btn-block btn-lg btn-success"  onClick={()=>{
						this.setState(_.set({...this.state},'modals.conclude.show',true));
					}}>Conclude Drug Test</button>
				</div>
			</div>
			<Modal size="md" isOpen={_.get(this.state,'modals.conclude.show',false)} toggle={()=>{ this.setState(_.set({...this.state},'modals.conclude.show', false)); }}>
				<ModalBody>
					<div className="card">
						<div className="card-block p-2">
							<button className="btn btn-lg btn-block btn-success">Test Complete</button>
							<button className="btn btn-lg btn-block btn-danger">Test Incomplete</button>
						</div>
					</div>
				</ModalBody>
			</Modal>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
		event:_.get(state,'session.event',false),
		test:_.get(state,'session.test',false),
	};
})(PageComponent);

