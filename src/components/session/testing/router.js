import React from 'react';
import _ from 'lodash';

import { Route, Link } from 'react-router-dom';

import { connect } from 'react-redux';

import { Page as DashboardPage } from './dashboard/page';
import { Page as PersonalPage } from './personal/page';
import { Page as MedicationPage } from './medication/page';
import { Page as ConsentPage } from './consent/page';
import { Page as AlcoholPage } from './alcohol/page';
import { Page as DrugPage } from './drug/page';

class ContainerComponent extends React.Component {
	render() {
		return(<div>
			<div className="mb-3">
				<h3>Testing Event</h3>
				<div>#F64A3B0B-AB85-4577-961A-948E6A8EEABF</div>
				<hr className="my-3" />
			</div>
			<div className="card mb-3">
				<div className="card-block p-2 text-center">
					<h4>Joesph Doe, 16/05/1968</h4>
					<div>Billy's Contracting, #RGM-32331</div>
				</div>
			</div>
			{this.props.children}
		</div>);
	}
}

const Container = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
	};
})(ContainerComponent);

export const Router = ({ match }) => (
	<Container>
		<Route exact path={match.url} component={DashboardPage} />
		<Route exact path={`${match.url}/personal`} component={PersonalPage} />
		<Route exact path={`${match.url}/medication`} component={MedicationPage} />
		<Route exact path={`${match.url}/consent`} component={ConsentPage} />
		<Route exact path={`${match.url}/alcohol/:uuid`} component={AlcoholPage} />
		<Route exact path={`${match.url}/drug/:uuid`} component={DrugPage} />
	</Container>
);