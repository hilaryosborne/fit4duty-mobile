import React from 'react';
import _ from 'lodash';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class PageComponent extends React.Component {

	state = {};

	render() {
		return (<div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Personal Details</h4>
					<hr />
					<div className="form-group row">
						<div className="col col-12 col-sm-6">
							<label>First Name</label>
							<input type="text" className="form-control" />
						</div>
						<div className="col col-12 col-sm-6">
							<label>Surname</label>
							<input type="text" className="form-control" />
						</div>
					</div>
					<div className="form-group mb-0">
						<label>Date Of Birth</label>
						<input type="text" className="form-control" />
					</div>
				</div>
			</div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Personal Details</h4>
					<hr />
					<div className="form-group row">
						<div className="col col-12 col-sm-6">
							<label>Employment Type</label>
							<select className="form-control">
								<option>CONTRACTOR</option>
								<option>EMPLOYEE</option>
							</select>
						</div>
						<div className="col col-12 col-sm-6">
							<label>Employment Details</label>
							<input type="text" className="form-control" />
						</div>
					</div>
					<div className="form-group mb-0">
						<label>Service/Employee #</label>
						<input type="text" className="form-control" />
					</div>
				</div>
			</div>
			<div className="card">
				<div className="card-block p-2">
					<div className="row">
						<div className="col col-12 col-sm-4">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.props.dispatch(push('.'));
							}}>Back</button>
						</div>
						<div className="col col-12 col-sm-8">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.props.dispatch(push('medication'));
							}}>Ok, Continue</button>
						</div>
					</div>
				</div>
			</div>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
		event:_.get(state,'session.event',false),
		test:_.get(state,'session.test',false),
	};
})(PageComponent);

