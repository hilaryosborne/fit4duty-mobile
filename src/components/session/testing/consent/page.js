import React from 'react';
import _ from 'lodash';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class PageComponent extends React.Component {

	state = {};

	render() {
		return (<div>
			<div className="alert alert-success mb-3 p-2">
				<h4>Step Sign Off</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mollis velit ac velit efficitur venenatis. Pellentesque id purus vulputate lorem elementum iaculis.</p>
				<button className="btn btn-block btn-lg btn-success" onClick={()=>{
					this.props.dispatch(push('.'));
				}}>Complete Details</button>
			</div>
			<div className="card">
				<div className="card-block p-2">
					<div className="row">
						<div className="col col-12 col-sm-4">
							<button className="btn btn-block btn-secondary" onClick={()=>{
								this.props.dispatch(push('.'));
							}}>Back</button>
						</div>
						<div className="col col-12 col-sm-8">
							<button className="btn btn-block  btn-secondary" onClick={()=>{
								this.props.dispatch(push('.'));
							}}>Ok, Continue</button>
						</div>
					</div>
				</div>
			</div>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		session:_.get(state,'session',false),
		collector:_.get(state,'session.collector',false),
		event:_.get(state,'session.event',false),
		test:_.get(state,'session.test',false),
	};
})(PageComponent);

