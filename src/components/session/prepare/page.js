import React from 'react';
import _ from 'lodash';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class PageComponent extends React.Component {

	constructor(props){
		super(props);
		this.state = {};
	}

	render() {
		return (<div>
			<div className="card mb-2">
				<div className="card-block p-2">
					<h4>Client Representative Notification</h4>
					<hr />
					<div className="alert alert-info p-2">
						<p>Nunc auctor consequat ligula, id tristique elit accumsan non. Mauris varius dignissim metus blandit mattis. Maecenas nec fringilla magna.</p>
					</div>
					<ul className="list-group mb-2">
						<li className="d-block py-1 px-2 list-group-item">
							<div className="d-flex">Notification Attempt<span className="ml-auto">Tick</span></div>
							<small><strong>Staff:</strong> Brian Someone <strong>Time:</strong> 4:56pm, 16/04/2033, <strong>Method:</strong> Mobile</small>
						</li>
						<li className="d-block py-1 px-2 list-group-item">
							<div className="d-flex">Notification Attempt<span className="ml-auto">Tick</span></div>
							<small><strong>Staff:</strong> Brian Someone <strong>Time:</strong> 4:56pm, 16/04/2033, <strong>Method:</strong> Mobile</small>
						</li>
					</ul>
					<button className="btn btn-block btn-primary">Add Notification</button>
				</div>
			</div>
			<div className="card mb-2">
				<div className="card-block p-2">
					<h4>Preparation Checklist</h4>
					<hr />
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt leo at euismod dictum. Nulla ultrices tortor quam, ac molestie lectus placerat sed.</p>
					<ul className="list-group">
						<li className="d-flex py-1 px-2 list-group-item">Completed all required tests<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item">Collected all waste matter<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item">Packed and correctly stored equipment<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item">Liaised with appropriate site staff<span className="ml-auto">!</span></li>
					</ul>
					<hr />
					<button className="btn btn-block btn-lg btn-success">Conclude Preperation</button>
				</div>
			</div>
			<div className="card">
				<div className="card-block p-2">
					<div className="row">
						<div className="col col-12 col-sm-4">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.props.dispatch(push('.'));
							}}>Back</button>
						</div>
						<div className="col col-12 col-sm-8">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.props.dispatch(push('medication'));
							}}>Ok, Continue</button>
						</div>
					</div>
				</div>
			</div>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',{}),
		session:_.get(state,'session',{})
	};
})(PageComponent);

