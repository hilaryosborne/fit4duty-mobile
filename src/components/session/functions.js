import _ from 'lodash';

export const getSessionCollector = function(session,collector) {
	return _.find(_.get(session,'data.collectors',[]),(_collector)=>{
		return _.get(_collector,'staff_uuid',false) === _.get(collector,'data._uuid');
	});
};