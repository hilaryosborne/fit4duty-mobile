import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class PageComponent extends React.Component {

	state = {};

	getSessions() {
		return _.get(this.props,'schedule.stream.data',[]);
	}

	loadSession(session) {
		this.props.dispatch(push('/session/'+_.get(session,'_uuid')+'/'));
	}

	render() {
		return (<div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>My Session List</h4>
					<hr />
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt leo at euismod dictum. Nulla ultrices tortor quam, ac molestie lectus placerat sed.</p>
					<ul className="list-group mb-2">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{  }}>View All Sessions<span className="ml-auto">x</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{  }}>View Pending Sessions<span className="ml-auto">x</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{  }}>View Pending Sessions<span className="ml-auto">x</span></li>
					</ul>
					<ul className="list-group">
						{_.map(this.getSessions(),(session,k)=>{
							return (<li key={k} className="d-block py-1 px-2 list-group-item" onClick={this.loadSession.bind(this,session)}>
								<div>TESTING SESSION</div>
								<small>#{_.get(session,'_uuid')}</small>
							</li>);
						})}
					</ul>
				</div>
			</div>
			<div className="card">
				<div className="card-block p-2">
					<div className="row">
						<div className="col col-12 col-sm-4">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.props.dispatch(push('..'));
							}}>Back</button>
						</div>
						<div className="col col-12 col-sm-8">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.props.dispatch(push('..'));
							}}>Ok, Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		schedule:_.get(state,'schedule',{}),
		session:_.get(state,'session',{}),
	};
})(PageComponent);

