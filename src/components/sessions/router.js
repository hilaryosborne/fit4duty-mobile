import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import { Route, Link } from 'react-router-dom';

import { Page as DashboardPage } from './dashboard/page';

class ContainerComponent extends React.Component {
	render() {
		return(<div>
			<div className="mb-3">
				<h3 className="d-flex">Testing Sessions <a onClick={()=>{ this.props.dispatch(push('../')); }} className="ml-auto">Back</a></h3>
				<hr className="my-3" />
			</div>
			{this.props.children}
		</div>);
	}
}

const Container = connect((state)=>{
	return {
		profile:_.get(state,'profile',false)
	};
})(ContainerComponent);

export const Router = ({match}) => (
	<Container>
		<div>
			<Route exact path={match.url} component={DashboardPage} />
		</div>
	</Container>
);