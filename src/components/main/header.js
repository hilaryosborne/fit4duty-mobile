import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class HeaderComponent extends React.Component {

	state = {};

	defaultProps = {
		className:''
	};

	render() {
		return (<div className="app-header">
			<div className="d-flex">
				<h3 onClick={()=>{ this.props.dispatch(push('/')); }} className="mb-0">Fit4Duty</h3>
				<div className="ml-auto">
					<span onClick={()=>{ this.props.dispatch(push('/calendar/')); }}>Calendar</span>
					<span onClick={()=>{ this.props.dispatch(push('/sessions/')); }}>Sessions</span>
					<span onClick={()=>{ this.props.dispatch(push('/profile/')); }}>Profile</span>
				</div>
			</div>
			<hr className="mt-2 mb-3" />
		</div>);
	}

}

export const Header = connect((state)=>{
	return {};
})(HeaderComponent);

