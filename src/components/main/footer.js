import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class FooterComponent extends React.Component {

	state = {};

	defaultProps = {
		className:''
	};

	render() {
		return (<div className="app-footer">
			<hr className="mt-3 mb-3" />
			<div className="d-flex">
				<h3 onClick={()=>{ this.props.dispatch(push('/')); }} className="mb-0">Fit4Duty</h3>
				<div className="ml-auto">
					<span onClick={()=>{ this.props.dispatch(push('/calendar/')); }}>Calendar</span>
					<span onClick={()=>{ this.props.dispatch(push('/sessions/')); }}>Sessions</span>
					<span onClick={()=>{ this.props.dispatch(push('/profile/')); }}>Profile</span>
				</div>
			</div>
			<hr className="mt-3 mb-3" />
			<div className="text-center">
				<p>Copyright Fit4Duty Pty Ltd. All rights Reserved</p>
			</div>
		</div>);
	}

}

export const Footer = connect((state)=>{
	return {};
})(FooterComponent);

