import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class BreadcrumbComponent extends React.Component {

	state = {};

	defaultProps = {
		className:''
	};

	render() {
		return (<ol className={classnames('breadcrumb',this.props.className)}>
			<li onClick={()=>{ this.props.dispatch(push('/')); }} className="breadcrumb-item active">Home</li>
			{this.props.children}
		</ol>);
	}

}

export const Breadcrumb = connect((state)=>{
	return {};
})(BreadcrumbComponent);

