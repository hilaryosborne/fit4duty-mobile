import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

import { Header } from './header';
import { Footer } from './footer';

import { SessionsRouter } from "../sessions/index";
import { SessionRouter } from "../session/index";
import { CalendarRouter } from "../calendar/index";
import { InventoryRouter } from "../inventory/index";
import { ProfileRouter } from "../profile/index";
import { SecurityRouter } from "../security/index";
import { DashboardPage } from "../dashboard/index";

import { store } from '../../index';
import { history } from '../../index';

import { PROFILE_SYNC } from '../../actions/profile';
import { SCHEDULE_SYNC } from '../../actions/schedule';

export class LobbyComponent extends React.Component {

	buffer = {};

	ping_profile = 5000;

	ping_schedule = 25000;

	componentDidMount() {
		this.doPingProfile();
		this.doPingSchedule();
	}

	doPingProfile() {
		store.dispatch(PROFILE_SYNC());
	}

	doPingSchedule() {
		store.dispatch(SCHEDULE_SYNC());
	}

	render() {
		return(<Provider store={store}>
			<ConnectedRouter history={history}>
				<div className="card" style={{width:768,margin:'1rem auto 2rem auto'}}>
					<div className="card-block">
						<Header />
						<div className="app-body">
							<Route exact path="/" component={DashboardPage}/>
							<Route exact path="/calendar" component={CalendarRouter}/>
							<Route exact path="/security" component={SecurityRouter}/>
							<Route path="/profile" component={ProfileRouter}/>
							<Route path="/inventory" component={InventoryRouter}/>
							<Route path="/sessions" component={SessionsRouter}/>
							<Route path="/sessions/:filter" component={SessionsRouter}/>
							<Route path="/session/:uuid" component={SessionRouter}/>
						</div>
						<Footer />
					</div>
				</div>
			</ConnectedRouter>
		</Provider>);
	}

}
