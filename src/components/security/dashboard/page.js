import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class PageComponent extends React.Component {

	state = {};

	render() {
		return (<div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Update Password</h4>
					<hr />
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse accumsan eu ligula sit amet varius. Donec elementum dolor eu nibh semper sagittis nec in elit.</p>
					<div className="form-group row mb-2">
						<div className="col col-12 col-sm-6">
							<label>Password</label>
							<input type="text" className="form-control" />
						</div>
						<div className="col col-12 col-sm-6">
							<label>Password Again</label>
							<input type="text" className="form-control" />
						</div>
					</div>
					<div className="form-group mb-0">
						<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
							this.props.dispatch(push('..'));
						}}>Update</button>
					</div>
				</div>
			</div>
			<div className="card">
				<div className="card-block p-2">
					<div className="row">
						<div className="col col-12 col-sm-4">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.props.dispatch(push('..'));
							}}>Back</button>
						</div>
						<div className="col col-12 col-sm-8">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.props.dispatch(push('..'));
							}}>Ok, Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',false)
	};
})(PageComponent);

