import React from 'react';
import _ from 'lodash';
import { Route, Link } from 'react-router-dom';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import { Page as DashboardPage } from './dashboard/page';

class ContainerComponent extends React.Component {
	render() {
		return(<div>
			{this.props.children}
		</div>);
	}
}

const Container = connect((state)=>{
	return {
		profile:_.get(state,'profile',false)
	};
})(ContainerComponent);

export const Router = ({match}) => (
	<Container>
		<div>
			<Route exact path={match.url} component={DashboardPage} />
		</div>
	</Container>
);