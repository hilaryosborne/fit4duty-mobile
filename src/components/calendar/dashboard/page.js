import React from 'react';
import _ from 'lodash';
import moment from 'moment';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import BigCalendar from 'react-big-calendar';
import { Breadcrumb as MainBreadcrumb } from '../../main/breadcrumb';

// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.momentLocalizer(moment); // or globalizeLocalizer

const EVENTS = [
	{
		'title': 'All Day Event',
		'start': new Date(2017, 5, 10, 17, 0, 0, 0),
		'end': new Date(2017, 5, 10, 19, 30, 0, 0),
		'desc': 'Big conference for important people'
	},
	{
		'title': 'All Day Event',
		'start': new Date(2017, 5, 8, 10, 0, 0, 0),
		'end': new Date(2017, 5, 8, 13, 30, 0, 0),
		'desc': 'Big conference for important people'
	}
];

class PageComponent extends React.Component {

	state = {};

	getEvents() {
		const c_sessions = _.get(this.props,'schedule.stream.data',[]);

		return _.map(c_sessions,(session)=>{

			const date = _.get(session,'expected.date');
			const duration = _.get(session,'expected.duration');

			return {
				'title':'Testing Session',
				'start': moment(date).toDate(),
				'end': moment(date).add(duration,'minutes').toDate(),
				'_session':session
			};
		});
	}

	render() {
		return (<div>
			<MainBreadcrumb className="mb-3">
				<li className="breadcrumb-item active">Calendar</li>
			</MainBreadcrumb>
			<div className="card">
				<div className="card-block p-2">
					<BigCalendar
						selectable
						events={this.getEvents()}
						defaultView='week'
						onSelectEvent={(item)=>{ this.props.dispatch(push('/session/'+_.get(item,'_session._uuid')+'/')); }}
						onSelectSlot={_.noop}
					/>
				</div>
			</div>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',false),
		schedule:_.get(state,'schedule',{})
	};
})(PageComponent);

