import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';

import { connect } from 'react-redux';

class DirtyComponent extends React.Component {

	state = {
		is_visible:true
	};

	defaultProps = {
		className:''
	};

	doDismiss() {
		const state = {...this.state};
		_.set(state,'is_visible',false);
		this.setState(state);
	}

	render() {
		return (this.state.is_visible ? <div className={classnames('alert alert-warning alert-dirty p-2', this.props.className)}>
			<h4 className="d-flex">{_.get(this.props,'title','Warning! Unsaved Changes')}<span onClick={this.doDismiss.bind(this)} className="ml-auto">Dismiss</span></h4>
			{this.props.children}
		</div> : false);
	}

}

export const Dirty = connect((state)=>{
	return {};
})(DirtyComponent);

