import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';

import { connect } from 'react-redux';

import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class UpdateComponent extends React.Component {

	state = {
		is_open:false
	};

	defaultProps = {
		className:'',
		onConfirm:_.noop,
		onCancel:_.noop,
		listener:false
	};

	componentDidMount() {
		if (this.props.listener) {
			this.props.listener.addListener('show', this.doShowAction.bind(this));
			this.props.listener.addListener('hide', this.doHideAction.bind(this));
		}
	}

	doToggle() {
		const state = {...this.state};
		_.set(state,'is_open',!_.get(state,'is_open'));
		this.setState(state);
	}

	doShowAction() {
		const state = {...this.state};
		_.set(state,'is_open',true);
		this.setState(state);
	}

	doHideAction() {
		const state = {...this.state};
		_.set(state,'is_open',false);
		this.setState(state);
	}

	doUpdateAction() {
		this.doHideAction();
		this.props.onConfirm();
	}

	render() {
		return (<Modal isOpen={this.state.is_open} toggle={this.doToggle.bind(this)} className={this.props.className}>
			<ModalHeader toggle={this.doToggle.bind(this)}>{_.get(this.props,'title','Confirm Content Update?')}</ModalHeader>
			{this.props.children ?
				<ModalBody>
					{this.props.children}
				</ModalBody> : false}
			<ModalFooter>
				<Button color="primary" onClick={this.doUpdateAction.bind(this)}>Confirm Update</Button>{' '}
				<Button color="secondary" onClick={this.doToggle.bind(this)}>Cancel</Button>
			</ModalFooter>
		</Modal>);
	}

}

export const Update = connect((state)=>{
	return {};
})(UpdateComponent);

