import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class DetailsComponent extends React.Component {

	state = {};

	defaultProps = {
		draft:false,
		onUpdate:_.noop
	};

	getFieldValue(path,placeholder) {
		return _.get(this.props.draft,'data.'+path,placeholder);
	}

	doFieldUpdate(path,el) {
		const draft = {...this.props.draft};
		_.set(draft,'data.'+path,el.target.value);
		this.props.onUpdate(draft);
	}

	render() {
		return (<div>
			<div className="form-group row">
				<div className="col col-12 col-sm-3">
					<label>Title</label>
					<select
						value={this.getFieldValue('details.title')}
						onChange={this.doFieldUpdate.bind(this,'details.title')}
						className="form-control">
						<option>--</option>
					</select>
				</div>
				<div className="col col-12 col-sm-9">
					<label><strong>Given Names</strong> <span className="required">*</span></label>
					<input
						type="text"
						value={this.getFieldValue('details.given_names')}
						onChange={this.doFieldUpdate.bind(this,'details.given_names')}
						className="form-control" />
				</div>
			</div>
			<div className="form-group row">
				<div className="col col-12 col-sm-12">
					<label><strong>Surname</strong> <span className="required">*</span></label>
					<input
						type="text"
						value={this.getFieldValue('details.surname')}
						onChange={this.doFieldUpdate.bind(this,'details.surname')}
						className="form-control" />
				</div>
			</div>
			<div className="form-group row mb-0">
				<div className="col col-12 col-sm-6">
					<label>Date Of Birth</label>
					<input
						type="date"
						value={this.getFieldValue('details.dob')}
						onChange={this.doFieldUpdate.bind(this,'details.dob')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-6">
					<label>Gender</label>
					<select
						value={this.getFieldValue('details.gender')}
						onChange={this.doFieldUpdate.bind(this,'details.gender')}
						className="form-control">
						<option value>--</option>
						<option value="MALE">Male</option>
						<option value="FEMALE">Female</option>
					</select>
				</div>
			</div>
		</div>);
	}

}

export const Details = connect((state)=>{
	return {
		profile:_.get(state,'profile',false)
	};
})(DetailsComponent);
