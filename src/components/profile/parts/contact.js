import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class ContactComponent extends React.Component {

	state = {};

	defaultProps = {
		draft:false,
		onUpdate:_.noop
	};

	getFieldValue(path,placeholder) {
		return _.get(this.props.draft,'data.'+path,placeholder);
	}

	doFieldUpdate(path,el) {
		const draft = {...this.props.draft};
		_.set(draft,'data.'+path,el.target.value);
		this.props.onUpdate(draft);
	}

	render() {
		return (<div>
			<div className="form-group row">
				<div className="col col-12 col-sm-2">
					<label>Country</label>
					<input
						type="text"
						value={this.getFieldValue('contact.primary_phone.country')}
						onChange={this.doFieldUpdate.bind(this,'contact.primary_phone.country')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-2">
					<label>Area</label>
					<input
						type="text"
						value={this.getFieldValue('contact.primary_phone.area')}
						onChange={this.doFieldUpdate.bind(this,'contact.primary_phone.area')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-6">
					<label><strong>Primary Number</strong> <span className="required">*</span></label>
					<input
						type="text"
						value={this.getFieldValue('contact.primary_phone.number')}
						onChange={this.doFieldUpdate.bind(this,'contact.primary_phone.number')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-2">
					<label>Ext.</label>
					<input
						type="text"
						value={this.getFieldValue('contact.primary_phone.extension')}
						onChange={this.doFieldUpdate.bind(this,'contact.primary_phone.extension')}
						className="form-control" />
				</div>
			</div>
			<div className="form-group row">
				<div className="col col-12 col-sm-2">
					<label>Country</label>
					<input
						type="text"
						value={this.getFieldValue('contact.secondary_phone.country')}
						onChange={this.doFieldUpdate.bind(this,'contact.secondary_phone.country')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-2">
					<label>Area</label>
					<input
						type="text"
						value={this.getFieldValue('contact.secondary_phone.area')}
						onChange={this.doFieldUpdate.bind(this,'contact.secondary_phone.area')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-6">
					<label><strong>Primary Number</strong> <span className="required">*</span></label>
					<input
						type="text"
						value={this.getFieldValue('contact.secondary_phone.number')}
						onChange={this.doFieldUpdate.bind(this,'contact.secondary_phone.number')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-2">
					<label>Ext.</label>
					<input
						type="text"
						value={this.getFieldValue('contact.secondary_phone.extension')}
						onChange={this.doFieldUpdate.bind(this,'contact.secondary_phone.extension')}
						className="form-control" />
				</div>
			</div>
			<hr className="my-3"/>
			<div className="form-group">
				<label>Primary Email Address</label>
				<input
					type="text"
					value={this.getFieldValue('contact.primary_email')}
					onChange={this.doFieldUpdate.bind(this,'contact.primary_email')}
					className="form-control" />
			</div>
			<div className="form-group mb-0">
				<label>Secondary Email Address</label>
				<input
					type="text"
					value={this.getFieldValue('contact.secondary_email')}
					onChange={this.doFieldUpdate.bind(this,'contact.secondary_email')}
					className="form-control" />
			</div>
		</div>);
	}

}

export const Contact = connect((state)=>{
	return {
		profile:_.get(state,'profile',false)
	};
})(ContactComponent);
