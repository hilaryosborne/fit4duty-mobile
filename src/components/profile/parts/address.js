import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class AddressComponent extends React.Component {

	state = {};

	defaultProps = {
		draft:false,
		onUpdate:_.noop
	};

	getFieldValue(path,placeholder) {
		return _.get(this.props.draft,'data.'+path,placeholder);
	}

	doFieldUpdate(path,el) {
		const draft = {...this.props.draft};
		_.set(draft,'data.'+path,el.target.value);
		this.props.onUpdate(draft);
	}

	render() {
		return (<div>
			<div className="form-group row">
				<div className="col col-12 col-sm-3">
					<label>Street No.</label>
					<input
						type="text"
						value={this.getFieldValue('address.primary.street_no')}
						onChange={this.doFieldUpdate.bind(this,'address.primary.street_no')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-5">
					<label>Street Name</label>
					<input
						type="text"
						value={this.getFieldValue('address.primary.street_name')}
						onChange={this.doFieldUpdate.bind(this,'address.primary.street_name')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-4">
					<label>Street Type</label>
					<select
						value={this.getFieldValue('address.primary.street_type')}
						onChange={this.doFieldUpdate.bind(this,'address.primary.street_type')}
						className="form-control">
						<option>--</option>
					</select>
				</div>
			</div>
			<div className="form-group row">
				<div className="col col-12 col-sm-4">
					<label>Suburb</label>
					<input
						type="text"
						value={this.getFieldValue('address.primary.suburb')}
						onChange={this.doFieldUpdate.bind(this,'address.primary.suburb')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-4">
					<label>State</label>
					<select
						value={this.getFieldValue('address.primary.state')}
						onChange={this.doFieldUpdate.bind(this,'address.primary.state')}
						className="form-control">
						<option>--</option>
					</select>
				</div>
				<div className="col col-12 col-sm-4">
					<label>Postcode</label>
					<input
						type="text"
						value={this.getFieldValue('address.primary.postcode')}
						onChange={this.doFieldUpdate.bind(this,'address.primary.postcode')}
						className="form-control" />
				</div>
			</div>
			<hr className="my-3" />
			<div className="form-group row">
				<div className="col col-12 col-sm-6">
					<label>Longitude</label>
					<input
						type="text"
						value={this.getFieldValue('address.primary.longitude')}
						onChange={this.doFieldUpdate.bind(this,'address.primary.longitude')}
						className="form-control" />
				</div>
				<div className="col col-12 col-sm-6">
					<label>Latitude</label>
					<input
						type="text"
						value={this.getFieldValue('address.primary.latitude')}
						onChange={this.doFieldUpdate.bind(this,'address.primary.latitude')}
						className="form-control" />
				</div>
			</div>
			<div className="form-group row mb-0">
				<div className="col col-12 col-sm-12">
					<button className="btn btn-block btn-secondary">Show Map</button>
				</div>
			</div>
		</div>);
	}

}

export const Address = connect((state)=>{
	return {
		profile:_.get(state,'profile',false)
	};
})(AddressComponent);
