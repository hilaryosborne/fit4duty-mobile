import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import { Address } from '../parts/address';
import { Contact } from '../parts/contact';
import { Details } from '../parts/details';
import { EventEmitter } from 'fbemitter';
import { Breadcrumb as MainBreadcrumb } from '../../main/breadcrumb';

import { Dirty as BlockDirtyAlert } from '../../alerts/block/dirty';
import { ConfirmUpdateModal } from "../../alerts/index";

import { PROFILE_UPDATE } from '../../../actions/profile';

class PageComponent extends React.Component {

	state = {
		draft:false,
		is_dirty:false,
		is_loading:false,
		is_saving:false,
		is_error:false
	};

	emitters = {
		confirm_update:new EventEmitter()
	};

	componentDidMount() {
		const state = {...this.state};
		_.set(state,'draft',{
			data:_.get(this.props,'profile.stream.data[0]',false),
			included:_.get(this.props,'profile.stream.included',false)
		});
		this.setState(state);
	}

	onDraftUpdate(draft) {
		const state = {...this.state};
		_.set(state,'draft',draft);
		_.set(state,'is_dirty',true);
		this.setState(state);
	}

	onDraftSave() {
		// Create the draft to update
		const payload = {
			// Return data to json format
			data:[
				{...this.state.draft.data}
			],
			// Duplicate included array
			included:[...this.state.draft.included]
		};
		// Initiate the upload
		this.props.dispatch(PROFILE_UPDATE(payload));
	}

	render() {
		return (<div>
			<MainBreadcrumb className="mb-3">
				<li className="breadcrumb-item active">Profile</li>
			</MainBreadcrumb>
			{_.get(this.state,'is_dirty',false) ?
				<BlockDirtyAlert className="mb-3">
					<hr />
				</BlockDirtyAlert>: false }
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Personal Details</h4>
					<hr />
					<Details
						draft={_.get(this.state,'draft')}
						onUpdate={this.onDraftUpdate.bind(this)} />
				</div>
			</div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Contact Details</h4>
					<hr />
					<Contact
						draft={_.get(this.state,'draft')}
						onUpdate={this.onDraftUpdate.bind(this)} />
				</div>
			</div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>Physical Address</h4>
					<hr />
					<Address
						draft={_.get(this.state,'draft')}
						onUpdate={this.onDraftUpdate.bind(this)} />
				</div>
			</div>
			<div className="card">
				<div className="card-block p-2">
					<div className="row">
						<div className="col col-12 col-sm-12">
							<button className="btn btn-block btn-lg btn-secondary" onClick={()=>{
								this.emitters.confirm_update.emit('show');
							}}>Ok, Save</button>
						</div>
					</div>
				</div>
			</div>
			<ConfirmUpdateModal
				listener={this.emitters.confirm_update}
				onConfirm={this.onDraftSave.bind(this)}/>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',false)
	};
})(PageComponent);
