import React from 'react';
import _ from 'lodash';
import { Route, Link } from 'react-router-dom';

import { Page as AssetsPage } from './assets/page';
import { Page as StockPage } from './stock/page';
import { Page as DashboardPage } from './dashboard/page';

import { connect } from 'react-redux';

class ContainerComponent extends React.Component {
	render() {
		return(<div>
			{this.props.children}
		</div>);
	}
}

const Container = connect((state)=>{
	return {
		profile:_.get(state,'profile',false)
	};
})(ContainerComponent);

export const Router = ({match}) => (
	<Container>
		<div>
			<Route exact path={match.url} component={DashboardPage} />
			<Route path={`${match.url}/asset/:uuid`} component={AssetsPage} />
			<Route path={`${match.url}/stock/:uuid`} component={StockPage} />
		</div>
	</Container>
);