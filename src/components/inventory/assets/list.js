import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class ListComponent extends React.Component {

	state = {};

	render() {
		return (
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4 className="mb-2">Assets List</h4>
					<hr />
				</div>
			</div>
		);
	}

}

export const List = connect((state)=>{
	return {
		profile:_.get(state,'profile',false)
	};
})(ListComponent);

