import React from 'react';
import _ from 'lodash';

import { List as AssetsList } from '../assets/list';
import { List as StockList } from '../stock/list';

import { connect } from 'react-redux';

class PageComponent extends React.Component {

	constructor(props){
		super(props);
		this.state = {};
	}

	render() {
		return (<div>
			<AssetsList />
			<StockList />
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',{})
	};
})(PageComponent);

