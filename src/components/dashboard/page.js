import React from 'react';
import _ from 'lodash';

import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import {PROFILE_SYNC} from "../../actions/profile";
import {SCHEDULE_SYNC} from "../../actions/schedule";

class PageComponent extends React.Component {

	constructor(props){
		super(props);
		this.state = {};
	}

	doProfileSync() {
		this.props.dispatch(PROFILE_SYNC());
	}

	doScheduleSync() {
		this.props.dispatch(SCHEDULE_SYNC());
	}

	render() {
		return (<div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>My Profile</h4>
					<hr />
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt leo at euismod dictum. Nulla ultrices tortor quam, ac molestie lectus placerat sed.</p>
					<ul className="list-group mb-2">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('./profile/')); }}>Manage Profile<span className="ml-auto">x</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('./security/')); }}>Manage Security<span className="ml-auto">x</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.doProfileSync(); }}>Sync Profile<span className="ml-auto">x</span></li>
					</ul>
					<ul className="list-group">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('./inventory/')); }}>Manage Inventory<span className="ml-auto">x</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.doScheduleSync(); }}>Sync Schedule<span className="ml-auto">x</span></li>
					</ul>
				</div>
			</div>
			<div className="card mb-3">
				<div className="card-block p-2">
					<h4>My Sessions</h4>
					<hr />
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt leo at euismod dictum. Nulla ultrices tortor quam, ac molestie lectus placerat sed.</p>
					<ul className="list-group mb-0">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('./calendar/')); }}>View Calendar<span className="ml-auto">x</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ this.props.dispatch(push('./sessions/')); }}>View Sessions<span className="ml-auto">{_.get(this.props,'schedule.stream.data',[]).length}</span></li>
					</ul>
				</div>
			</div>
			<hr />
			<div className="card">
				<div className="card-block p-2">
					<h4>Logs And Reporting</h4>
					<hr />
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt leo at euismod dictum. Nulla ultrices tortor quam, ac molestie lectus placerat sed.</p>
					<ul className="list-group">
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ }}>Report An Incident<span className="ml-auto">!</span></li>
						<li className="d-flex py-1 px-2 list-group-item" onClick={()=>{ }}>Report Broken Equipment<span className="ml-auto">!</span></li>
					</ul>
				</div>
			</div>
		</div>);
	}

}

export const Page = connect((state)=>{
	return {
		profile:_.get(state,'profile',{}),
		schedule:_.get(state,'schedule',{})
	};
})(PageComponent);

