import React from 'react';

import _ from 'lodash';

import {SessionForm} from "../../components/index";

import DUMMY_DATA from "../../data/job";

export class Route extends React.Component {

	constructor(props){
		super(props);
		this.state = {};
	}

	render() {
		return (<div>
			<SessionForm />
		</div>);
	}
}