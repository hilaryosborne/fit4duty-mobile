export default function update(state = {}, action) {
	switch(action.type) {
		case 'UPDATE_VIEW':
			return action.payload;
		default:
			return state
	}
}