export default function update(state = {
	flash:{},
	stream:false
}, action) {
	switch(action.type) {
		case 'SYNC_SCHEDULE':
			return action.payload;
		default:
			return state
	}
}