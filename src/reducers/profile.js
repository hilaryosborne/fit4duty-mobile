import stream from '../data/profile.json';

export default function update(state = {
	flash:{},
	stream:stream
}, action) {
	switch(action.type) {
		case 'UPDATE_PROFILE':
			return action.payload;
		case 'SYNC_PROFILE':
			return action.payload;
		default:
			return state
	}
}