export default function update(state = {
	flash:{},
	stream:false,
	loaded:false,
	collector:false,
	event:false
}, action) {
	switch(action.type) {
		case 'UPDATE_FLASH':
			return action.payload;
		case 'LOAD_SESSION':
			return action.payload;
		case 'UPDATE_SESSION':
			return action.payload;
		case 'PERSIST_SESSION':
			return action.payload;
		case 'CLEAR_SESSION':
			return {};
		case 'UPDATE_COLLECTOR':
			return action.payload;
		case 'UPDATE_EVENT':
			return action.payload;
		default:
			return state
	}
}