import _ from 'lodash';
import axios from "axios";

const AUTHED_URL = 'http://localhost:8080';

export const doApiGetRequest = (uri)=>{
	// Return the action
	return function(dispatch, getState) {
		// Retrieve the stored token, if we have one
		const token = 'ASDASDASDASDASDASDASDASD';
		// Create the axios client
		const client = axios.create({
			timeout: 15000,
			headers: {'Authorization':token}
		});
		// Attempt to post the data to the resource proxy
		return client.get(AUTHED_URL+'/'+uri)
			.then((response)=>{
				if (_.get(response,'data.success') !== true) { throw 'API error'; }
				return _.get(response,'data');
			});
	}
};

export const doApiPostRequest = (uri,payload)=>{
	// Return the action
	return function(dispatch, getState) {
		// Retrieve the stored token, if we have one
		const token = 'ASDASDASDASDASDASDASDASD';
		// Create the axios client
		const client = axios.create({
			timeout: 15000,
			headers: {'Authorization':token}
		});
		// Attempt to post the data to the resource proxy
		return client.post(AUTHED_URL+'/'+uri, payload)
	}
};