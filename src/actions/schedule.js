import {doApiGetRequest} from "./api";

export const SCHEDULE_SYNC = () => {
	// Return the action
	return function(dispatch, getState) {
		return dispatch(
			doApiGetRequest('api/schedule/')
		).then((response)=>{
			const state = getState();
			// Make a clone of the schedule state
			const schedule = {...state.schedule};
			// Update the schedule transmission data
			schedule.transimission = {
				recieved:Date.now(), status:1
			};
			// Update the stream contents
			schedule.stream = response.payload;
			// Send the dispatch
			dispatch({type:'SYNC_SCHEDULE',payload:schedule});
		}).catch(()=>{
			const state = getState();
			const schedule = {...state.schedule};
			// Update the schedule transmission data
			schedule.transimission = {
				recieved:Date.now(), status:1
			};
			// Send the dispatch
			dispatch({type:'SYNC_SCHEDULE',payload:schedule});
		});
	}
};
