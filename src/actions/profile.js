import {doApiGetRequest,doApiPostRequest} from "./api";

export const PROFILE_SYNC = () => {
	// Return the action
	return function(dispatch, getState) {
		return dispatch(
			doApiGetRequest('api/profile/')
		).then((response)=>{
			const state = getState();
			const profile = {...state.profile};
			profile.transimission = {
				recieved:Date.now(),
				status:1
			};
			profile.stream = response.payload; console.log(profile);
			dispatch({type:'SYNC_PROFILE',payload:profile});
		}).catch(()=>{
			const state = getState();
			const profile = {...state.profile};
			profile.transimission = {
				recieved:Date.now(),
				status:0
			};
			dispatch({type:'SYNC_PROFILE',payload:profile});
		});
	}
};

export const PROFILE_UPDATE = (stream)=>{
	// Return the action
	return function(dispatch, getState) {
		return dispatch(
			doApiPostRequest('api/profile/update', stream)
		).then((response)=>{
			console.log('response');
		}).catch(()=>{
			console.log('Nope');
		});
	}
};
