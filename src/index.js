import React from 'react'
import ReactDOM from 'react-dom'

import createHistory from 'history/createHashHistory';
import thunk from 'redux-thunk';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { LobbyComponent } from "./components/main/lobby";

// Create a history of your choosing (we're using a browser history in this case)
export const history = createHistory();
// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history);

import './assets/scss/main.scss';

import * as reducers from './reducers';

export const store = createStore(
	combineReducers({router:routerReducer, ...reducers}),
	applyMiddleware(middleware,thunk)
);

ReactDOM.render(<LobbyComponent />, document.getElementById('root'));
